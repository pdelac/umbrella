package com.foo.umbrella.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.foo.umbrella.R;

public class AppPreferences extends AppCompatActivity {
  private static final String TAG = AppPreferences.class.getSimpleName() + "_TAG";
  FragmentManager fragmentManager;
  FragmentTransaction fragmentTransaction;
  SettingsFragment settingsFragment;
  SharedPreferences sharePrefZip;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_app_preferences);
//    sharePrefZip = PreferenceManager.getDefaultSharedPreferences(this);
//    SharedPreferences.Editor editor = sharePrefZip.edit();

//    editor.putString("zip", "07524");
//    editor.apply();
    //toolbar thingy magic
    // my_child_toolbar is defined in the layout file
    Toolbar myChildToolbar =
            (Toolbar) findViewById(R.id.pref_toolbar);
    setSupportActionBar(myChildToolbar);

    // Get a support ActionBar corresponding to this toolbar
    ActionBar ab = getSupportActionBar();

    // Enable the Up button
    ab.setDisplayHomeAsUpEnabled(true);

    fragmentManager = getFragmentManager();
    fragmentTransaction = fragmentManager.beginTransaction();

    fragmentTransaction.replace(android.R.id.content, new SettingsFragment(), TAG);

  }
  public static class SettingsFragment extends PreferenceFragment
  {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.app_preferences);
    }

    @Override
    public void onPause() {
      super.onPause();

      addPreferencesFromResource(R.xml.app_preferences);
    }
  }
}
