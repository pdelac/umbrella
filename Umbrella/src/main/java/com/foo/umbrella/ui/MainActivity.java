package com.foo.umbrella.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foo.umbrella.R;
import com.foo.umbrella.UmbrellaApp;
import com.foo.umbrella.data.ApiServicesProvider;
import com.foo.umbrella.data.model.WeatherData;

import java.util.ArrayList;

import hugo.weaving.DebugLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = MainActivity.class.getSimpleName() + "_TAG"; //this helps dynamically identify in what context TAG is being used.
  ApiServicesProvider provider;
  MyRecyclerViewAdapter myAdapter;
  RecyclerView recyclerView;
  GridLayoutManager layoutManager;

  Context mContext;
  TextView locationTV;
  TextView todayTempTV;
  TextView todayConditionTV;
  SharedPreferences sharePref;
  String zip;
  String unit;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    provider = new ApiServicesProvider(UmbrellaApp.getApplication());


    // data to populate the RecyclerView with
// set up the RecyclerView
    recyclerView = (RecyclerView) findViewById(R.id.rv_Information);
    layoutManager = new GridLayoutManager(MainActivity.this, 4);
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(layoutManager);

    //Pref Stuff
    sharePref = PreferenceManager.getDefaultSharedPreferences(this);
    zip = sharePref.getString("zip", "");
    unit = sharePref.getString("units", "");
    setWeather(zip);

    //ViewStuff
    locationTV = (TextView) findViewById(R.id.tv_location);
    todayTempTV = (TextView) findViewById(R.id.tv_today_temp);
    todayConditionTV = (TextView) findViewById(R.id.tv_condition);

    //toolbar thingy
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  public void setWeather(String zip) {
    final WeatherData[] responseWD = new WeatherData[1];
    Call<WeatherData> call = provider.getWeatherService().forecastForZipCallable(zip);
    call.enqueue(new Callback<WeatherData>() {
      @Override
      public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
        if (response.isSuccessful()) {
          responseWD[0] = response.body();
          setViews(responseWD[0]);
        } else {
          Toast.makeText(MainActivity.this, "API Response Error:" + response.code(), Toast.LENGTH_SHORT).show();
        }
      }

      @Override
      public void onFailure(Call<WeatherData> call, Throwable t) {
        Toast.makeText(MainActivity.this, "error : {", Toast.LENGTH_SHORT).show();
      }
    });
  }

  public void setBackground(String tempF, String tempC) {
    AppBarLayout appBarLayout;
    LinearLayout linearLayout;
    appBarLayout = (AppBarLayout) findViewById(R.id.appBarWeatherInfo);
    linearLayout = (LinearLayout) findViewById(R.id.weather_information);

    if (Double.parseDouble(tempF) >= 60 || Double.parseDouble(tempC) >= 15.5) {
      appBarLayout.setBackgroundColor(getResources().getColor(R.color.weather_warm));
      linearLayout.setBackgroundColor(getResources().getColor(R.color.weather_warm));
    } else {
      appBarLayout.setBackgroundColor(getResources().getColor(R.color.weather_cool));
      linearLayout.setBackgroundColor(getResources().getColor(R.color.weather_cool));

    }
  }

  private void setViews(WeatherData wd) {
    String tempF = wd.getCurrentObservation().getTempFahrenheit();
    String tempC = wd.getCurrentObservation().getTempCelsius();
    setBackground(tempF, tempC);
    tempF = tempF + "\u00B0";
    tempC = tempC + "\u00b0";
    String condition = wd.getCurrentObservation().getWeatherDescription();
    String location = wd.getCurrentObservation().getDisplayLocation().getFullName();


    if (parseInt(unit) == 0){
      todayTempTV.setText(tempF);
    } else {
      todayTempTV.setText(tempC);
    }
    todayConditionTV.setText(condition);
    locationTV.setText(location);
    myAdapter = new MyRecyclerViewAdapter(MainActivity.this, wd);
    recyclerView.setAdapter(myAdapter);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.action_bar_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menu_item_new_thingy) {
      Intent intent = new Intent(this, AppPreferences.class);
      startActivity(intent);
    }

    return super.onOptionsItemSelected(item);
  }


}
