package com.foo.umbrella.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foo.umbrella.R;
import com.foo.umbrella.data.model.WeatherData;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pdelac on 9/18/17.
 */

  public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyHolder> {

  private Context context;
  WeatherData wd;


  public MyRecyclerViewAdapter(Context context, WeatherData wd) {
    this.context = context;
    this.wd = wd;
  }

  @Override
    public MyHolder onCreateViewHolder (ViewGroup parent, int viewType){
    View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_items, null);
    MyHolder myHolder = new MyHolder(layout);
    return myHolder;
  }

  @Override
  public void onBindViewHolder(MyHolder holder, int position) {
//    holder.img.setImageResource(images[position]);
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
      final Date dateObj = sdf.parse(wd.getForecast().get(position).getDisplayTime());
      holder.timeTV.setText(new SimpleDateFormat("K:mm a").format(dateObj));
    } catch (final ParseException e) {
      e.printStackTrace();
    }

    switch (wd.getForecast().get(position).getCondition()) {
      case "Snowy Rainy":
        holder.conditionIV.setImageResource(R.drawable.weather_snowy_rainy);
        break;
      case "Rain":
        holder.conditionIV.setImageResource(R.drawable.weather_rainy);
        break;
      case "Clear":
        holder.conditionIV.setImageResource(R.drawable.weather_sunny);
        break;
      case "Partly Cloudy":
        holder.conditionIV.setImageResource(R.drawable.weather_partlycloudy);
        break;
      case "Mostly Cloudy":
        holder.conditionIV.setImageResource(R.drawable.weather_cloudy);
        break;
      case "Windy Variant":
        holder.conditionIV.setImageResource(R.drawable.weather_windy_variant);
        break;
      default:
        holder.conditionIV.setImageResource(R.drawable.weather_sunny);
        break;
    }
    holder.tempTV.setText(wd.getForecast().get(position).getTempFahrenheit() + "\u00B0");

  }

  @Override
  public int getItemCount() {
    return wd.getForecast().size();
  }

  public static class MyHolder extends RecyclerView.ViewHolder{
    ImageView conditionIV;
    TextView timeTV;
    TextView tempTV;
    public MyHolder(View itemView) {
      super(itemView);
      conditionIV = (ImageView) itemView.findViewById(R.id.iv_forecast_image);

      tempTV = (TextView) itemView.findViewById(R.id.tv_temp_forecast);
      timeTV = (TextView) itemView.findViewById(R.id.tv_time_forecast);
    }
  }

}
